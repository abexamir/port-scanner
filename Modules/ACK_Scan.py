#! /usr/bin/python3
# ACK_Scan.py - Python3 based ACK scanner.  A SYN/ACK packet is sent through a raw socket.
# Does not rely on scapy, rather creates its own packets and raw sockets.
# Must be run as root.

""" 
    -sA (TCP ACK scan) (from nmap.org)

    This scan is different than the others discussed so far\
    in that it never determines open (or even open|filtered) ports.\
    It is used to map out firewall rulesets, determining whether they are stateful or not and which ports are filtered.\
    The ACK scan probe packet has only the ACK flag set (unless you use --scanflags).\
    When scanning unfiltered systems, open and closed ports will both return a RST packet.\
    Nmap then labels them as unfiltered, meaning that they are reachable by the ACK packet,\
    but whether they are open or closed is undetermined.\
    Ports that don't respond, or send certain ICMP error messages back (type 3, code 0, 1, 2, 3, 9, 10, or 13),\
    are labeled filtered.
"""

import socket
import random
import sys
from struct import *
import argparse


# checksum functions
def checksum(msg):
    s = 0
    # loop taking 2 characters at a time
    for i in range(0, len(msg), 2):
        w = (msg[i] << 8) + (msg[i + 1])
        s = s + w

    s = (s >> 16) + (s & 0xffff)
    # complement and mask to 4 byte short
    s = ~s & 0xffff
    return s


def create_socket():
    # create a raw socket
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_TCP)
    except socket.error:
        sys.exit()

    # tell kernel not to put in headers since we are providing it
    s.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)
    return s


def create_ip_header(source_ip, dest_ip):
    # ip header fields
    header_length = 5
    version = 4
    tos = 0
    tot_len = 20 + 20
    id = random.randrange(18000, 65535, 1)
    frag_off = 0
    ttl = 255
    protocol = socket.IPPROTO_TCP
    check = 10
    saddr = socket.inet_aton(source_ip)
    daddr = socket.inet_aton(dest_ip)
    hl_version = (version << 4) + header_length
    ip_header = pack("!BBHHHBBH4s4s", hl_version, tos, tot_len, id, frag_off, ttl, protocol, check, saddr, daddr)
    return ip_header


def create_tcp_ACK_header(source_ip, dest_ip, dest_port):
    # tcp header fields
    source = random.randrange(32000, 62000, 1)  # source port
    seq = 0
    ack_seq = 65
    doff = 5
    # tcp flags
    fin = 0
    syn = 0
    rst = 0
    psh = 0
    ack = 1
    urg = 0
    window = socket.htons(8192)  # maximum window size
    check = 0
    urg_ptr = 0
    offset_res = (doff << 4) + 0
    tcp_flags = fin + (syn << 1) + (rst << 2) + (psh << 3) + (ack << 4) + (urg << 5)
    tcp_header = pack("!HHLLBBHHH", source, dest_port, seq, ack_seq, offset_res, tcp_flags, window, check, urg_ptr)
    # pseudo header fields
    source_address = socket.inet_aton(source_ip)
    dest_address = socket.inet_aton(dest_ip)
    placeholder = 0
    protocol = socket.IPPROTO_TCP
    tcp_length = len(tcp_header)
    psh = pack("!4s4sBBH", source_address, dest_address, placeholder, protocol, tcp_length)
    psh = psh + tcp_header
    tcp_checksum = checksum(psh)

    # make the tcp header again and fill in the correct checksum
    tcp_header = pack("!HHLLBBHHH", source, dest_port, seq, ack_seq, offset_res, tcp_flags, window, tcp_checksum,
                      urg_ptr)
    return tcp_header


def range_scan(source_ip, dest_ip, start_port, end_port):
    RST_ack_received = []  # store the list of UNfilterd ports here
    Not_RST = []
    # final full packet - FIN packets don"t have any data
    for j in range(start_port, end_port):
        s = create_socket(source_ip, dest_ip)
        s.settimeout(20)
        ip_header = create_ip_header(source_ip, dest_ip)
        tcp_header = create_tcp_ACK_header(source_ip, dest_ip, j)
        packet = ip_header + tcp_header
        s.sendto(packet, (dest_ip, 0))
        data = s.recvfrom(1024)[0][0:]
        ip_header = packet[14:20 + 14]
        IPV4_IP = unpack('!BBHHHBBH4s4s', ip_header)
        protocol = IPV4_IP[6]
        if protocol == 1:
            # DO Nothing
            Not_RST.append(j)
            print("port {} filterd".format(j))
        elif protocol == 6:
            ip_header_len = (data[0] & 0x0f) * 4
            ip_header_ret = data[0: ip_header_len - 1]
            tcp_header_len = (data[32] & 0xf0) >> 2
            tcp_header_ret = data[ip_header_len:ip_header_len + tcp_header_len - 1]
            if tcp_header_ret[13] == 0x004:  # RST flags set
                RST_ack_received.append(j)
                print("port {} Not filterd".format(j))
            else:
                Not_RST.append(j)
                print("port {} filterd".format(j))
        else:
            Not_RST.append(j)
    return RST_ack_received, Not_RST


def ack_scan(ipdest, start, stop):
    Unfilterd_port_list = []
    filtered_port_list = []
    step = 1
    ipsource = socket.gethostbyname("localhost")
    scan_ports = list(range(start, stop, step))
    if scan_ports[len(scan_ports) - 1] < stop:
        scan_ports.append(stop)
    for i in range(len(scan_ports) - 1):
        opl, fil = range_scan(ipsource, ipdest, scan_ports[i], scan_ports[i + 1])
        Unfilterd_port_list.append(opl)
        filtered_port_list.append(fil)
    for i in range(len(Unfilterd_port_list)):
        for j in range(len(Unfilterd_port_list[i])):
            print(Unfilterd_port_list[i][j], ", ")



# setup commandline arguments
parser = argparse.ArgumentParser(description='ACK Scanner')
parser.add_argument('--host', action="store", dest="host", default='localhost')
parser.add_argument('--startport', action="store", dest="start_port", default=1, type=int)
parser.add_argument('--endport', action="store", dest="end_port", default=1000, type=int)

# parse arguments
given_args = parser.parse_args()
host, start_port, end_port = socket.gethostbyname(given_args.host), given_args.start_port, given_args.end_port
ack_scan(host, start_port, end_port)
