#! /usr/bin/python3
# Connect_Scan.py - Python3 based Connect scanner.  Simple port scanner program.


"""
3.1  TCP connect() Scan [-sT] (from nmap.org)

These scans are so called because UNIX sockets programming uses a system call named connect()\
to begin a TCP connection to a remote site. If connect() succeeds, a connection was made.\
If it fails, the connection could not be made (the remote system is offline, the port is closed,\
or some other error occurred along the way). This allows a basic type of port scan,\
which attempts to connect to every port in turn, and notes whether or not the connection succeeded.\
Once the scan is completed, ports to which a connection could be established are listed as open,\
the rest are said to be closed.
"""

import socket
import argparse
from threading import Thread

def connect_scan(host, start_port, end_port):
    open_ports = []
    closed_ports = []
    threads = [] 
    def scan_ports(port):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex((host,port))
        # print ("working on port > {}".format(port))
        if result == 0:
            open_ports.append(port)
            #print((str(port))+' -> open') 
            sock.close()
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        else:
            closed_ports.append(port)
            #print((str(port))+' -> close') 
            sock.close()
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    for port in range(start_port,end_port):  
        t = Thread(target=scan_ports, args=(port,))
        threads.append (t)
        t.start()


    print("There are {} ports open on {} in this range: {} ".format(len(open_ports), host, open_ports))


# setup commandline arguments
parser = argparse.ArgumentParser(description='Port Scanner')
parser.add_argument('--host', action="store", dest="host", default='localhost')
parser.add_argument('--startport', action="store", dest="start_port", default=1, type=int)
parser.add_argument('--endport', action="store", dest="end_port", default=1000, type=int)

# parse arguments
given_args = parser.parse_args()
host, start_port, end_port = socket.gethostbyname(given_args.host), given_args.start_port, given_args.end_port
connect_scan(host, start_port, end_port)


