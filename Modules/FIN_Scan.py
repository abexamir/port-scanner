#! /usr/bin/python3
# FIN_Scan.py - Python3 based FIN scanner. 
# Does not rely on scapy, rather creates its own packets and raw sockets.
# Must be run as root.


import socket
import random
import sys
from struct import *
import argparse


# checksum functions
def checksum(msg):
    s = 0
    # loop taking 2 characters at a time
    for i in range(0, len(msg), 2):
        w = (msg[i] << 8) + (msg[i + 1])
        s = s + w

    s = (s >> 16) + (s & 0xffff)
    # complement and mask to 4 byte short
    s = ~s & 0xffff
    return s


def create_socket():
    # create a raw socket
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_TCP)
    except socket.error:
        sys.exit()

    # tell kernel not to put in headers since we are providing it
    s.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)
    return s


def create_ip_header(source_ip, dest_ip):

    # ip header fields
    headerlen = 5
    version = 4
    tos = 0
    tot_len = 20 + 20
    id = random.randrange(18000, 65535, 1)
    frag_off = 0
    ttl = 255
    protocol = socket.IPPROTO_TCP
    check = 10
    saddr = socket.inet_aton(source_ip)
    daddr = socket.inet_aton(dest_ip)
    hl_version = (version << 4) + headerlen
    ip_header = pack("!BBHHHBBH4s4s", hl_version, tos, tot_len, id, frag_off, ttl, protocol, check, saddr, daddr)
    return ip_header



def create_tcp_FIN_header(source_ip, dest_ip, dest_port):
    # tcp header fields
    source = random.randrange(32000, 62000, 1)  # source port
    seq = 0
    ack_seq = 0
    doff = 5
    # tcp flags
    fin = 1
    syn = 0
    rst = 0
    psh = 0
    ack = 0
    urg = 0
    window = socket.htons(8192)  # maximum window size
    check = 0
    urg_ptr = 0
    offset_res = (doff << 4) + 0
    tcp_flags = fin + (syn << 1) + (rst << 2) + (psh << 3) + (ack << 4) + (urg << 5)
    tcp_header = pack("!HHLLBBHHH", source, dest_port, seq, ack_seq, offset_res, tcp_flags, window, check, urg_ptr)
    # pseudo header fields
    source_address = socket.inet_aton(source_ip)
    dest_address = socket.inet_aton(dest_ip)
    placeholder = 0
    protocol = socket.IPPROTO_TCP
    tcp_length = len(tcp_header)
    psh = pack("!4s4sBBH", source_address, dest_address, placeholder, protocol, tcp_length)
    psh = psh + tcp_header
    tcp_checksum = checksum(psh)

    # make the tcp header again and fill in the correct checksum
    tcp_header = pack("!HHLLBBHHH", source, dest_port, seq, ack_seq, offset_res, tcp_flags, window, tcp_checksum,
                      urg_ptr)
    return tcp_header


def range_scan(source_ip, dest_ip, start_port, end_port):
    RST_ack_received = []  # store the list of close ports here
    NOT_RST = []
    # final full packet - FIN packets don"t have any data
    for j in range(start_port, end_port):
        s = create_socket(source_ip, dest_ip)
        s.settimeout(220)
        ip_header = create_ip_header(source_ip, dest_ip)
        tcp_header = create_tcp_FIN_header(source_ip, dest_ip, j)
        packet = ip_header + tcp_header
        s.sendto(packet, (dest_ip, 0))
        data = s.recvfrom(1024)[0][0:]
        ip_header = packet[14:20 + 14]
        IPV4_IP = unpack('!B B H H H B B H 4s 4s', ip_header)
        protocol = IPV4_IP[6]
        if protocol == 1:
            # ICMP should close
            RST_ack_received.append(j)
            print("port {}: closed".format(j))
        elif protocol == 6:
            ip_header_len = (data[0] & 0x0f) * 4
            ip_header_ret = data[0: ip_header_len - 1]
            tcp_header_len = (data[32] & 0xf0) >> 2
            tcp_header_ret = data[ip_header_len:ip_header_len + tcp_header_len - 1]
            if tcp_header_ret[13] > 0x003:  # RST flags set
                RST_ack_received.append(j)
                print("port {}: closed".format(j))
            else:
                NOT_RST.append(j)
                print("port {}: open".format(j))
    return RST_ack_received, NOT_RST


def fin_scan(ipdest, start, stop):
    open_port_list = []
    closed_port_list = []
    step = 1
    ipsource = socket.gethostbyname("localhost")
    scan_ports = list(range(start, stop, step))
    if scan_ports[len(scan_ports) - 1] < stop:
        scan_ports.append(stop)
    for i in range(len(scan_ports) - 1):
        closel, opl = range_scan(ipsource, ipdest, scan_ports[i], scan_ports[i + 1])
        closed_port_list.append(closel)
        open_port_list.append(opl)


# setup commandline arguments
parser = argparse.ArgumentParser(description='FIN Scanner')
parser.add_argument('--host', action="store", dest="host", default='localhost')
parser.add_argument('--startport', action="store", dest="start_port", default=1, type=int)
parser.add_argument('--endport', action="store", dest="end_port", default=1000, type=int)

# parse arguments
given_args = parser.parse_args()
host, start_port, end_port = socket.gethostbyname(given_args.host), given_args.start_port, given_args.end_port
fin_scan(host, start_port, end_port)
