#! /usr/bin/python3
# Window_Scan.py - Python3 based Window scanner.
# Does not rely on scapy, rather creates its own packets and raw sockets.
# Must be run as root.

"""
 -sW (TCP Window scan) (from nmap.org)

    Window scan is exactly the same as ACK scan except that\
    it exploits an implementation detail of certain systems \
    to differentiate open ports from closed ones, rather than\
    always printing unfiltered when a RST is returned. It does\
    this by examining the TCP Window field of the RST packets\
    returned. On some systems, open ports use a positive window\
    size (even for RST packets) while closed ones have a zero window.\
    So instead of always listing a port as unfiltered when it receives\
    a RST back, Window scan lists the port as open or closed if the TCP\
    Window value in that reset is positive or zero, respectively.
"""

import socket
import random
import sys
from struct import *
import argparse


# checksum functions - direct from Silver Moon
def checksum(msg):
    s = 0
    # loop taking 2 characters at a time
    for i in range(0, len(msg), 2):
        w = (msg[i] << 8) + (msg[i + 1])
        s = s + w

    s = (s >> 16) + (s & 0xffff)
    # complement and mask to 4 byte short
    s = ~s & 0xffff
    return s


def create_socket():
    # create a raw socket
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_TCP)
    except socket.error:
        sys.exit()

    # tell kernel not to put in headers since we are providing it
    s.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)
    return s


def create_ip_header(source_ip, dest_ip):
    # ip header fields
    headerlen = 5
    version = 4
    tos = 0
    tot_len = 20 + 20
    id = random.randrange(18000, 65535, 1)
    frag_off = 0
    ttl = 255
    protocol = socket.IPPROTO_TCP
    check = 10
    saddr = socket.inet_aton(source_ip)
    daddr = socket.inet_aton(dest_ip)
    hl_version = (version << 4) + headerlen
    ip_header = pack("!BBHHHBBH4s4s", hl_version, tos, tot_len, id, frag_off, ttl, protocol, check, saddr, daddr)
    return ip_header


def create_tcp_ACK_header(source_ip, dest_ip, dest_port):
    # tcp header fields
    source = random.randrange(32000, 62000, 1)  # source port
    seq = 0
    ack_seq = 65
    doff = 5
    # tcp flags
    fin = 0
    syn = 0
    rst = 0
    psh = 0
    ack = 1
    urg = 0
    window = socket.htons(8192)  # maximum window size
    check = 0
    urg_ptr = 0
    offset_res = (doff << 4) + 0
    tcp_flags = fin + (syn << 1) + (rst << 2) + (psh << 3) + (ack << 4) + (urg << 5)
    tcp_header = pack("!HHLLBBHHH", source, dest_port, seq, ack_seq, offset_res, tcp_flags, window, check, urg_ptr)
    # pseudo header fields
    source_address = socket.inet_aton(source_ip)
    dest_address = socket.inet_aton(dest_ip)
    placeholder = 0
    protocol = socket.IPPROTO_TCP
    tcp_length = len(tcp_header)
    psh = pack("!4s4sBBH", source_address, dest_address, placeholder, protocol, tcp_length)
    psh = psh + tcp_header
    tcp_checksum = checksum(psh)

    #make the tcp header again and fill in the correct checksum
    tcp_header = pack("!HHLLBBHHH", source, dest_port, seq, ack_seq, offset_res, tcp_flags, window, tcp_checksum, urg_ptr)
    return tcp_header


def range_scan(source_ip, dest_ip, start_port, end_port):
    RST_ack_received = []# store the list of UNFILTERD ports here
    OPEN_PORTS = [] #just_open_port
    # final full packet - FIN packets don't have any data
    for j in range(start_port, end_port):
        s = create_socket(source_ip, dest_ip)
        s.settimeout(20)
        ip_header = create_ip_header(source_ip, dest_ip)
        tcp_header = create_tcp_ACK_header(source_ip, dest_ip, j)
        packet = ip_header + tcp_header
        s.sendto(packet, (dest_ip, 0))
        data = s.recvfrom(1024)[0][0:]
        ip_header = packet[14:20 + 14]
        IPV4_IP = unpack('!BBHHHBBH4s4s', ip_header)
        protocol = IPV4_IP[6]
        if protocol == 1 :
            #DO Nothing
            print("port {}: closed filtered".format(j))
        elif protocol == 6:
            ip_header_len = (data[0] & 0x0f) * 4
            ip_header_ret = data[0: ip_header_len - 1]
            tcp_header_len = (data[32] & 0xf0) >> 2
            tcp_header_ret = data[ip_header_len:ip_header_len + tcp_header_len - 1]
            if tcp_header_ret[13] == 0x004:  # RST flags set
                RST_ack_received.append(j)
                print("port {}: not filtered".format(j))
                if tcp_header_ret[14] == 0 :
                    print("\tport {}: closed".format(j))
                else :
                    OPEN_PORTS.append(j)
                    print("\tport {}: open".format(j))
            else:
                print("port {}: filtered".format(j))
                if tcp_header_ret[14]==0 :
                    print("port {}: closed".format(j))
                else:
                    print("port {}: open".format(j))

    return RST_ack_received , OPEN_PORTS

def window_scan(ipdest, start, stop):
    Unfilterd_port_list = []
    filtered_port_list = []
    Open_port_list =[]
    step = 1
    ipsource = socket.gethostbyname("localhost")
    scan_ports = list(range(start, stop, step))
    if scan_ports[len(scan_ports) - 1] < stop:
        scan_ports.append(stop)
    for i in range(len(scan_ports) - 1):
        Unl , opl = range_scan(ipsource, ipdest, scan_ports[i], scan_ports[i + 1])
        Unfilterd_port_list.append(Unl)
        Open_port_list .append(opl)
    for i in range(len(Unfilterd_port_list)):
        for j in range(len(Unfilterd_port_list[i])):
            print(Unfilterd_port_list[i][j], ", ")



# setup commandline arguments
parser = argparse.ArgumentParser(description='ACK Scanner')
parser.add_argument('--host', action="store", dest="host", default='localhost')
parser.add_argument('--startport', action="store", dest="start_port", default=1, type=int)
parser.add_argument('--endport', action="store", dest="end_port", default=1000, type=int)

# parse arguments
given_args = parser.parse_args()
host, start_port, end_port = socket.gethostbyname(given_args.host), given_args.start_port, given_args.end_port
window_scan(host, start_port, end_port)

