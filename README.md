﻿# Port Scanner

# Scanner.py 
Python3 based scanner to carry out different types of port scanning.

# Modules

1. Connect Scan
2. ACK Scan
3. FIN Scan
4. SYN Scan
5. Windows Scan

# Usage
```bash
usage: Scanner.py [-sS] [-sU] [-sT] [sF] [-sA] [-sP Start Port] [-eP End port] [-t target]

optional arguments:
-sS, --synscan Enable this for SYN scan
-sA, --ackscan Enable this for ACK scan
-sT --connectscan Enable this for Connect scan
-sF --finscan Enable this for FIN scan
-sW --windowscan Enable this for Window scan
-sP --startport The first port to scan (Default: 0)
-eP --endport The last port to scan (Default: 1000)
-t --target Target host (Default: localhost)```

