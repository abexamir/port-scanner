#! /usr/bin/python3

# Scanner.py - Python3 based scanner to carry out different types of port scanning.
# Written with libraries: socket, sys, argparse, struct, os.
# The costume modules used (ACK_Scan.py, SYN_Scan.py, Window_Scan.py, FIN_Scan.py Connect_Scan.py) are written from base.
# Must be run as root.
# A project for Computer Networks I Course 98-2 semester at Isfahan University of Technology.
# Instructor: Dr. Manshaei.
# Programmed by Mohammad Serati Aligudarzi - Student. No: 9427363 - E-Mail: abexamir@gmail.com.


"""
usage: Scanner.py [-sS] [-sU] [-sT] [sF] [-sA] [-sP Start Port] [-eP End port] [-t target]

optional      arguments:
  -sS,       --synscan        Enable this for SYN scans
  -sA,       --ackscan        Enable this for ACK scans
  -sT        --connectscan    Enable this for Connect scans
  -sF        --finscan        Enable this for FIN scans
  -sW        --windowscan     Enable this for Window scans
  -sP        --startport      The first port to scan (Default: 0)
  -eP        --endport        The last port to scan (Default: 1000)
  -t         --target         Target host (Default: localhost)

"""

from Modules import ACK_Scan, SYN_Scan, Window_Scan, Connect_Scan, FIN_Scan
import argparse
import sys
import socket


parser = argparse.ArgumentParser(description='scanner.py - Replicates limited nmap functionality in python')
parser.add_argument('-sT', '--connectscan', action='store_true', dest="connectscan",
                    help='Enable this for SYN scan')
parser.add_argument('-sS', '--synscan', action='store_true', dest="synscan", help='Enable this for SYN scan')
parser.add_argument('-sA', '--ackscan', action='store_true', dest="ackscan", help='Enable this for ACK scans')
parser.add_argument('-sW', '--windowscan', action='store_true', dest="windowscan",
                    help='Enable this for Window scans')
parser.add_argument('-sF', '--finscan', action='store_true', dest="finscan", help='Enable this for fin scans')
parser.add_argument('-t', '--target', action="store", dest="target", help='The target you want to scan',
                    default='localhost')
parser.add_argument('-sP', '--startport', action="store", dest="startport", help='The first port to scan',
                    default=1, type=int)
parser.add_argument('-eP', '--endport', action="store", dest="endport", help='The last port to scan', default=1000,
                    type=int)

given_args = parser.parse_args()
target, start_port, end_port = socket.gethostbyname(given_args.target), given_args.startport, given_args.endport

if len(sys.argv) == 1: parser.print_help(); sys.exit(0)
args = parser.parse_args()

if args.synscan:
    print("SYN Scan on {} results:\n".format(target))
    SYN_Scan.syn_scan(target, start_port, end_port)
if args.ackscan:
    print("ACK Scan on {} results:\n".format(target))
    ACK_Scan.ack_scan(target, start_port, end_port)
if args.windowscan:
    print("Window Scan on {} results:\n".format(target))
    Window_Scan.window_scan(target, start_port, end_port)
if args.finscan:
    print("FIN Scan on {} results:\n".format(target))
    FIN_Scan.fin_scan(target, start_port, end_port)
if args.connectscan:
    print("Connect Scan on {} results:\n".format(target))
    Connect_Scan.connect_scan(target, start_port, end_port)

